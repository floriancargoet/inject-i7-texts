import path from "path";
import fs from "fs";
import minimist from "minimist";

// The asserts keyword tells TS that this function throws when test is false.
// We are exiting instead of throwing but the result is the same.
function assert(test: boolean, message: string): asserts test {
  if (!test) {
    console.log(message);
    process.exit(1);
  }
}

export function parseOptions(argv: Array<string>) {
  const args = minimist(argv.slice(2));

  assert(
    "story" in args && typeof args.story === "string",
    "A story is required."
  );
  assert(
    "google-doc-id" in args && typeof args["google-doc-id"] === "string",
    "A Google Doc ID is required."
  );

  const storyPath = path.resolve(process.cwd(), args.story, "Source/story.ni");
  assert(fs.existsSync(storyPath), "Story source file doesn't exist");

  return {
    storyPath,
    googleDocId: args["google-doc-id"],
  };
}
