#!/usr/bin/env node
import fs from "fs";
import Diff from "text-diff";
import chalk from "chalk";

function replace(str: string, start: number, end: number, newStr: string) {
  const first = str.slice(0, start);
  const last = str.slice(end);
  return first + newStr + last;
}

export function inject(sourcePath: string, texts: Record<string, string>) {
  const source = fs.readFileSync(sourcePath, "utf8");
  const namedStrings = getNamedStrings(source);
  console.log(namedStrings);
  let offset = 0;
  let modifiedCount = 0;
  let newSource = source;
  for (const [id, token] of Object.entries(namedStrings)) {
    const start = token.start + 1;
    const end = token.end - 1;
    const existing = token.value.slice(1, -1);
    let newText: string;
    if (!(id in texts)) {
      newText = existing;
      console.log(`${id}: not in external document.`);
    } else {
      newText = texts[id];
      if (existing !== newText) {
        modifiedCount++;
        console.log(`${id}: ${coloredDiff(computeDiff(existing, newText))}`);
      } else {
        console.log(`${id}: up-to-date.`);
      }
    }
    newSource = replace(newSource, start + offset, end + offset, newText);
    offset += newText.length - (end - start);
  }
  fs.writeFileSync(sourcePath, newSource);

  console.log(`Modified: ${modifiedCount}`);
}

function computeDiff(str1: string, str2: string) {
  const diff = new Diff();
  const result = diff.main(str1, str2);
  diff.cleanupSemantic(result);
  return result;
}

function coloredDiff(diffs: Array<[number, string]>) {
  return diffs
    .map(([op, text]) => {
      switch (op) {
        case 1:
          return chalk.green(text);
        case -1:
          return chalk.strikethrough.red(text);
        case 0:
          return text;
      }
    })
    .join("");
}

function getNamedStrings(source: string) {
  const tokens = parseI7(source);
  const namedStringsTokens = tokens.filter(
    (t) => t.type === "string" && t.comment
  );
  const namedStrings: Record<string, Token> = {};
  for (const t of namedStringsTokens) {
    const name = t.comment?.value.slice(1, -1) ?? "";
    namedStrings[name] = t;
  }
  return namedStrings;
}

type Token = {
  type: "comment" | "string";
  start: number;
  end: number;
  value: string;
  comment?: Token;
};

function parseI7(source: string) {
  const tokens: Array<Token> = [];
  let currentToken: Token | undefined;
  let i = 0;
  let inComment = false;
  let inString = false;
  let inSubstitution = false;

  function startToken(type: "string" | "comment", attachComment = false) {
    if (currentToken) throw new Error("token still in progress");
    currentToken = {
      type,
      start: i,
      end: source.length,
      value: "",
    };
    // attach previous comment if just before
    if (attachComment) {
      const lastToken = tokens[tokens.length - 1];
      if (lastToken?.type === "comment" && lastToken.end === i) {
        currentToken.comment = lastToken;
      }
    }
  }
  function endToken() {
    if (!currentToken) throw new Error("token missing");
    currentToken.end = i + 1;
    currentToken.value = source.slice(currentToken.start, currentToken.end);
    tokens.push(currentToken);
    currentToken = undefined;
  }
  for (i = 0; i < source.length; i++) {
    const c = source.charAt(i);
    switch (c) {
      case "[": {
        if (inComment) {
          // nested comment
        } else if (inString) {
          // substitution
          inSubstitution = true;
        } else {
          inComment = true;
          startToken("comment");
        }
        break;
      }
      case "]": {
        if (inComment) {
          inComment = false;
          endToken();
        } else if (inString) {
          // substitution
          inSubstitution = false;
        }
        break;
      }
      case '"': {
        if (inString) {
          inString = false;
          endToken();
        } else if (inComment) {
          // still in comment
        } else {
          inString = true;
          startToken("string", true);
        }
        break;
      }
    }
  }
  return tokens;
}
