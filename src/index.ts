#!/usr/bin/env node
import { inject } from "./inject.js";
import { getTextsFromGoogleDocs } from "./google-docs.js";
import { parseOptions } from "./options.js";

async function main() {
  const { storyPath, googleDocId } = parseOptions(process.argv)!;

  const textsMap = await getTextsFromGoogleDocs(googleDocId);

  inject(storyPath, textsMap);
}

main().catch(console.log);
